# Documentation How-to Guide

GitLab Static Pages is the new infrastructure for documentation site hosting which makes the creation of static sites easier. You can follow this documentation to migrate your site from the old infrastructure or to create a new site using Gitlab Pages. It is now possible to create documentation sites using different SSG (static site generators) e.g. Hugo, Jekyll, mkdocs etc.

1. [Create a new GitLab Static Pages site](gitlabpagessite/create_site.md).
2. [Migration from the old MkDocs sites](gitlabpagessite/migration.md).
3. [Modify your site's settings](gitlabpagessite/modify_site_settings.md).
4. [Review before deploying](gitlabpagessite/review.md).


Make sure to also have a look at the [advanced topics](advanced/index.md) for more advanced how-to guides.


## Contact

In case you want something different altogether or you just want to get in touch please contact us at  [documentation-contact@cern.ch](mailto:documentation-contact@cern.ch).