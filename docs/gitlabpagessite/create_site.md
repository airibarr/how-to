# Create a new MkDocs-based documentation site

With the steps listed below, you will be able to deploy your documentation site from your Gitlab Pages repository.

## Create a GitLab repository

First of all, you need to create a new repository on [GitLab](https://gitlab.cern.ch/){target=_blank} to host your documentation files.

The new infrastructure comes with a new nice feature, which allows you to select between several ways of writing your documentation. You can use one of the [Static Site Generators (SSGs) that GitLab offers as templates](create_site.md#option-a-create-a-documentation-site-using-gitlab-template) or continue using [Markdown along with MkDocs](create_site.md#option-b-create-a-site-using-mkdocs-markdown).

### Option A. Create a documentation site using a GitLab template

This solution is the simplest one since the repository will be preconfigured to use specific SSG (Static Site Generator) and the only thing for you to do will be to write the documentation content.

Create a new project by clicking on the upper right button **New project** and select the option **Create from template**. This will show the list of the templates which are available. **NOTICE** that some of these templates are not for static sites, so choose one of the following:

- Pages/Gatsby
- Pages/Hugo
- Pages/Jekyll
- Pages/Plain HTML
- Pages/GitBook
- Pages/Hexo
- Static Site Editor/Middleman
- Middleman project with Static Site Editor support

This list is continuously updated with the new alternatives available.

Once you know which SSG you would like to use just click on **Use template** button and fill in the form that will appear.

![create](/images/gitlab-create-template.png)

!!! warning
    The visibility level applies only to the git repository. This only affects who has access to the files in the repository.

* "Private" means you have to explicitly give read access to other users.
* "Internal" means all CERN users have read access.
* "Public" means that everyone on the Internet has read access.

Once you have filled in the form click on **Create project** button and the repository will be created with a documentation sample that you can easily edit.

More information about GitLab templates [here](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_new_project_template.html){target=_blank}.

#### Build artifacts

The last step is to build and push the artifacts which will be served to the users. For this, go to CI/CD section in the left panel of your repository and click on the Pipelines option. Once there, click on the upper right button **Run Pipeline** and wait until the job ends successfully.

![pipelines](/images/gitlab-pipelines-migration.png)

### Option B. Create a site using MkDocs (Markdown)

If you prefer to use the MkDocs with Markdown sources for your documentation, as it was done before, you should follow this [structure](https://www.mkdocs.org/user-guide/writing-your-docs/){target=_blank}.

If you want to start with an example MkDocs structure, continue by [importing an example project](create_site.md#1-import-project). If you are confident with structuring your git repository on your own, continue with [a blank project](create_site.md#2-blank-project).


#### 1. Import project

Create a new project by clicking on the upper right button **New project** and select the **Import project** option. This will show several importing sources. Choose the **Repo by URL** option. It will open a form to fill in the information:

* **Git repository URL**: Enter: `https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example.git`. This will help you start with a very simple example MkDocs structure with minimal content.
* **Username (optional)**: Leave empty.
* **Password (optional)**: Leave empty.
* **Mirror repository**: Leave unchecked.
* **Project name**: Enter a descriptive name for this git repository. For example "My new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise, we suggest you [create one](https://gitlab.cern.ch/groups/new){target=_blank} first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`. Please, we encourage you to use `docs` as the `Project slug` for static documentation sites, as you can see in the previous example.
* **Project description (optional)**: Enter a description of your git repository. For example "This is my new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitly give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

!!! warning
    The name and the URL of your git repository are completely independent of your site name and URL.

![form](/images/gitlab-import-repo.png)


Once you filled in the form, click on **Create project** and a repository will be created with the sample of a MarkDown site.


#### 2. Blank project

Create a new project by clicking on the upper right button **New project** and select the **Create blank project** option. This will open a form to fill in the information:

* **Project name**: Enter a descriptive name for this git repository. For example "My new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise, we suggest you [create one](https://gitlab.cern.ch/groups/new){target=_blank} first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`. Please, we encourage you to use `docs` as the `Project slug` for static documentation sites, as you can see in the previous example.
* **Project description (optional)**: Enter a description of your git repository. For example "This is my new GitLab Pages site". This will help you and other users remember what this git repository is for.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitly give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

![form](/images/gitlab-create-blank-repo.png)


Once you have filled the form, click on **Create project** and an empty repository will be created.


#### Build artifacts

If you opt for creating a blank project, the last step is to build and push the artifacts needed to deploy the site. If you want an easy and automatic way to do it we recommend you to [include a special template](https://gitlab.cern.ch/authoring/documentation/mkdocs-ci/-/blob/master/mkdocs-gitlab-pages.gitlab-ci.yml){target=_blank} on top of your `.gitlab-ci.yml` as you can see in the [example](https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example/-/blob/master/.gitlab-ci.yml){target=_blank}. This will build and push (to a special S3 bucket) static assets every time a new commit is pushed to the `master` branch (or a branch is merged to `master` branch). You can modify it to your liking. In case you opted for the [Import project](create_site.md#1-import-project) option your `.gitlab-ci.yml` will already include this template, so you will not have to perform this step.

If everything goes as expected you will see a new job in the CI/CD -> Pipelines page:

![pipelines](/images/gitlab-pipelines-migration.png)

---

!!! warning
    With this new setup all changes are automatically deployed once the Gitlab CI job rebuilds your static site.


## Create a new project on [webeos.cern.ch](https://webeos.cern.ch/){target=_blank}

Once your GitLab repository is fully configured, go to [webEOS](https://webeos.cern.ch/){target=_blank} and create a new project, check on the left side menu that the **Administrator** option is selected (and **not** Developer).

This will show a form with the following fields:

* **Name:** This is the name of your project. This field is mandatory. It is a good practice to use the same name as your GitLab's repository name, and end it with `-docs` (i.e. my-service-name-docs).
* **Display Name**: Enter the display name of the project.
* **Description**: Enter a description of your site. This field is mandatory.


## Create a site

Once your project is created, on the left bar do one of the following:

* go to the **Operators** -> **Installed Operators** if you are in the `Administrator` view
* or navigate to **+Add** -> **Operator backed** if you are in the `Developer` view

This will show the list of all available operators. Make sure that you are in the correct `Project`.

![installed_operators](/images/openshift-project-installed-operators.png)

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-create-site-operator.png)

To create your new site click on `Create instance` on the bottom of the box or go to the **Publish a static site from a Gitlan repository (gitlab.cern.ch)** tab in the top bar. This will show the list of your sites created in this project and a `Create GitlabPagesSites` button in the top-right part. Click on this button and it will open a form with the following fields:

* **Name:** This is the name of your site. Since this name has to be unique we recommend you to use the same name as you have used as a project name.
* **Labels (optional):** Optional labels to give more information about your site. You can leave it empty.
* **Host:** This is the custom domain that you will create in the [the next step](create_site.md#how-to-add-a-custom-domain) in the repository's GitLab Pages section and hence, the URL of your site, for example `migration-example.docs.cern.ch`. **Notice** that you do not have to put the `http/s` protocol at the beginning.
* **Anonymous:** It is a true/false toggle that will make your site accessible without authentication. If false it will redirect users to the CERN Single Sign-On in order to authenticate them before accessing your site.

!!! warning
    Remember that the `Host` has to be the same as one of the defined GitLab Pages custom domains, hence the same restrictions apply in order to create your hostname's site:

    * Domain (`migration-example` in the previous image) only allows the letters, numbers and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


Once you are done, click on **Create** and your new site will be shown in the list of your project's sites.

## How to enable Pages in a repository

The current repository that contains your documentation files will have a new `Settings` option called `Pages`

![settings](/images/gitlab-settings-pages.png)

Notice that to be able to see `Settings` you have to have, at least, a Maintainer role.

## How to add a custom domain

First of all, make sure that `Force HTTPS (requires valid certificates)` is ticked off and then click on the `Save changes` blue button. Your site will be accessible via HTTPS in any case thanks to the new infrastructure.

![certificates](/images/gitlab-https-pages.png)

In order to create your custom site's URL you have to click on the upper right button **New Domain**.
Now, to create your domain please, use the same URL that you used in the step before.

![domain](/images/gitlab-domain-new-site.png)

!!! warning
    There are some restrictions in order to create your domain:

    * Domain (`migration-example` in the previous image) only allows the letters, numbers and hyphen symbol ( - )
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


You can leave the **Certificate (PEM)** and **Key (PEM)** fields empty if you have previously disabled the button `Force HTTPS (requires valid certificates)`.

Click on **Create New Domain** button and then on the button **Save Changes**. If everything goes well you will see the new domain in the `Domains` list. Your new site should be available in a few minutes, so you can open your browser and type the URL you put as `Host` in [this section](create_site.md#create-a-site) to see your new site.
