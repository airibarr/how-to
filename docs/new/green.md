# Go <span style="color: green;">green</span>!

If you've reached the step it means your documentation is online and you're a happy person :-) What can make you even happier is making sure you use the right amount of precious CERN computing resources!

Serving a static documentation site is a fairly light task and we want to make sure that OpenShift limits the computing resources it uses to do so to the right amount.

#### A. Go to your OpenShift project

![Screenshot](/images/openshift-project-deployed-customised_URL.png)

---

#### B. Go to your OpenShift project Deployments

Hover over **Applications** on the left and click on **Deployments**.

![Screenshot](/images/openshift-project-deployments-do.png)

---

#### C. Go to your documentation deployment

Click on **docs** from the list of deployments. Unless you have a more complicated set-up you should only have one deployment in the list. The name **docs** might be different for you and it's essentially the **application name** you provided when you first [selected MkDocs and deployed your documentation](deploy.md#e-fill-in-the-information).

![Screenshot](/images/openshift-project-deployments-deployment-do.png)

---

#### D. Edit your documentation deployment's resource limits

Click on the **Actions** menu at the upper right corner and click on **Edit Resource Limits**.

![Screenshot](/images/openshift-project-deployments-deployment-resource_limits-do.png)

---

#### E. Fill in the information

* **CPU**
    * **Request**: Enter "10 millicores"
    * **Limit**: Enter "1 cores"
* **Memory**
    * **Request**: Enter "20 MiB"
    * **Limit**: Enter "100 MiB"

![Screenshot](/images/openshift-project-deployments-deployment-resource_limits-filled_in.png)

---

#### F. Click on **Save**

![Screenshot](/images/openshift-project-deployments-deployment-resource_limits-filled_in-do.png)

---

Mission accomplished, precious CERN computing resources have been saved!

Now that you're all done, make sure to also have a look at the [advanced topics](../advanced/index.md) for more advanced how-to guides (for example [how to make your service documentation available only to specific people/groups at CERN](../advanced/authentication_authorisation.md)).
