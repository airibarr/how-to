# Introduction

GitLab Static Pages is the new infrastructure for documentation site hosting which makes the creation of static sites easier. You can follow this documentation to migrate your site from the old infrastructure or to create a new site using Gitlab Pages. It is now possible to create documentation sites using different SSG (static site generators) e.g. Hugo, Jekyll, mkdocs etc.

1. [Create a new GitLab Static Pages site](create_site.md).
2. [Migration from the old MkDocs sites](migration.md).
3. [Modify your site's settings](modify_site_settings.md).
4. [Review before deploying](review.md).


## Contact

For any question or issue please, contact us at [documentation-contact@cern.ch](mailto:documentation-contact@cern.ch).
