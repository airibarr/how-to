# Temporarily review your documentation on OpenShift before deploying it: Extend

You can extend your review workflow by extending your CI / CD pipeline on GitLab.

During the [setup](setup.md) you created a new file in the root folder of your git repository called `.gitlab-ci.yml` with the following contents:

```yaml
include:
  - project: 'authoring/documentation/s2i-mkdocs-review'
    ref: master
    file: 'gitlab-ci-template.yml'
```

This essentially includes the entire pipeline from [`gitlab-ci-template.yml`](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-review/blob/master/gitlab-ci-template.yml) which comes with 2 predefined stages:

* `Deploy review`
* `Stop review`

You could add a custom stage of your own (while still keeping the predefined stages) by amending the contents of `.gitlab-ci.yml` like this:

```yaml
include:
  - project: 'authoring/documentation/s2i-mkdocs-review'
    ref: master
    file: 'gitlab-ci-template.yml'

stages:
  - Custom
  - Deploy review
  - Stop review

Custom extension:
  stage: Custom
  script:
    - echo 'Custom extension'
  only:
    - branches
  except:
    - master
```

This will execute the `Custom` stage before the `Deploy review` and `Stop review` stages. As a side effect the `Deploy review` and `Stop review` stages will not be executed if the `Custom` stage fails.

!!! note
    Note how we've instructed the `Custom` stage to be executed for all branches except for the `master` branch. The same rule applies to the predefined `Deploy review` and `Stop review` stages. We recommend you follow this rule unless you explicitly want to execute your custom stages for the `master` branch as well. Remember that when the `master` branch is updated [your documentation is automatically reployed anyway](../../new/redeploy.md). It is therefore important that you have correctly [entered the value of **Branch name or wildcard pattern to trigger on (leave blank for all)** as "master"](../../new/redeploy.md#h-fill-in-the-information).

As an example to this behaviour you can test the integrity of your documentation by test building it in a similar environment to the production one:

```yaml
include:
  - project: 'authoring/documentation/s2i-mkdocs-review'
    ref: master
    file: 'gitlab-ci-template.yml'

stages:
  - Test
  - Deploy review
  - Stop review

Test building:
  stage: Test
  image: gitlab-registry.cern.ch/authoring/documentation/s2i-mkdocs-container
  before_script:
    - export LC_ALL=en_US.utf-8
    - export LANG=en_US.utf-8
  script:
    - [ -f requirements.txt ] && pip install -r requirements.txt
    - mkdocs build --strict --site-dir $(mktemp -d)
  only:
    - branches
  except:
    - master
```

The `Test` stage essentially replicates the behaviour of the [S2I MkDocs Container](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-container) as it [builds](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-container/blob/master/assemble) your documentation. Note the usage of the `--strict` flag in this example that will make the build fail not only for errors but also for warnings.

Happy hacking :-)
