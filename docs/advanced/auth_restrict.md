# Restrict access to a part of your documentation

This guide allows to restrict *part of your documentation* to...

* Signed in users
* Specific CERN e-groups

!!! info
    In this example, we assume that you have an `internal` folder located in the `docs` folder. Anything in `docs` will be public, and any file under `internal` will require CERN Single-Sign on authentication, and optionally that the signed-in user is part of specific e-group(s)

## Restrict access to the entire documentation

To avoid redundancy, as a first step of this article, [restrict the entire documentation](authentication_authorisation.md) to a specific e-group.

## Change restricted location to `internal` folder

1. Go to [openshift.cern.ch](https://openshift.cern.ch) and open your project.
2. On the left menu, choose Resources/Config Maps:

    ![](../images/auth-restrict/auth1.png)

3. Select `cern-sso-proxy`:
   
    ![](../images/auth-restrict/auth2.png)

4. On the top right, select "Edit YAML":

    ![](../images/auth-restrict/auth3.png)

5. Focus on the highlighted text:

    ![](../images/auth-restrict/auth5.png)

    - Change `<Location "/">` to `<Location "/internal/">`, specifying the restricted path
    - Keep the line `Require valid-user` to restrict access to signed in users, and do **either of the following two options**:
        - **To restrict access to the `my-group` Cern e-group:** keep the line `Require shib-attr ADFS_GROUP my-group`
        
            OR

        - **To allow access to *any* signed-in user:** Delete the line `Require shib-attr ADFS_GROUP my-group`

6. Save changes to the YAML file.
7. To re-deply the proxy, choose Applications/Deployments in the left menu:

    ![](../images/auth-restrict/auth6.png)

8. Choose `cern-sso-proxy`:

    ![](../images/auth-restrict/auth7.png)

9. Click Deploy:

    ![](../images/auth-restrict/auth8.png)
