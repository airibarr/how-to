# How to delete a site

This section explains how you can remove your site. Since there are several stages that you may want to perform (from just removing the URL to removing all your resources), here you will see several subsections, so you can follow one or more depending on your needs.

!!! note
    In order to remove the site completely you need to perform all the steps described below.

## On [GitLab](https://gitlab.cern.ch){target=_blank}

### Remove your site's domain

In case that you want to unlink a specific Pages domain from your Gitlab repository, navigate to the page of your repository and, in the left panel go to the `Settings` option called `Pages`.

![settings](/images/gitlab-settings-pages.png)


Click on the red `Remove` button that is right next to the pages domain(s) you want to remove.

![remove_domain](/images/gitlab-remove-domain.png)


You can create this domain again later on and, if you did not change/remove anything else, you will end up in the same scenario as before, without the need of running the pipeline again since all the configuration and resources were not removed. This could be useful if you want to quickly hide your documentation site.

!!! warning
    If you remove the domain and then you create a different one remember that you will need to change also the `host` in your [webEOS](https://webeos.cern.ch){target=_blank} project. **NOTICE** that the new one may be already taken, so you might not be able to use it.


It is also possible to remove all custom domains at once by clicking the `Remove pages` button on the bottom of the page.

![remove_pages](/images/gitlab-remove-pages.png)


If you chose this option, notice that this will also remove your site resources. It means that in case you want to recreate your previous domain(s) you will need to run the pipeline again. For this, go to CI/CD section in the left panel of your repository and click on the Pipelines option. Once there, click on the upper right button **Run Pipeline** and wait until the job ends successfully.

![pipelines](/images/gitlab-pipelines-migration.png)


## On [webEOS](https://webeos.cern.ch/){target=_blank}

In order to remove your GitLab Pages Site from [webEOS](https://webeos.cern.ch/){target=_blank} select the project that holds your site. Once you are in the project, on the left bar, do one of the following:

* go to the **Operators** -> **Installed Operators** if you are in the `Administrator` view
* or navigate to **+Add** -> **Operator backed** if you are in the `Developer` view

This will show the list of all available operators. Make sure that you are in the correct `Project`.

![installed_operators](/images/openshift-project-installed-operators.png)

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-create-site-operator.png)

Go to the **Publish a static site from a Gitlan repository (gitlab.cern.ch)** tab in the top bar. This will show the list of your sites created in this project. Search for the `GitlabPagesSites` site that you want to remove and click on the three dots to select the `Delete GitlabPagesSite` option.

![delete_operator](/images/openshift-project-edit-site-operator.png)

This will open a confirmation message. Click on the red `Delete` button and your `GitlabPagesSite` site will be removed.

This action will just remove the `GitlabPagesSite`'s webeos configurations and resources for this site, so in case that you did not remove your GitLab Pages resources, you will be able to recreate your site again by clicking on the `Create GitlabPagesSites` button in the top-right part and filling the form again

!!! warning
    You can use different values for the `Name`, `Labels` and `Anonymous`, however, `Host` has to be the same as your previous site, since has to be the same as your GitLab Pages domain.