# Temporarily review changes to the documentation before deploying

A limitation of the new static sites is that doing temporary reviews of your site before deploying requires a few more steps.

!!! note
    Remember that before, in order to review a MR you were supposed to include a special Gitlab CI template. This template is not useful anymore thus remember to remove it from your Gitlab CI configuration.

## Fork your repository

For this, you need to [fork your repository](https://docs.gitlab.com/ee/user/project/working_with_projects.html#fork-a-project){target=_blank} and apply the changes on the forked repository. You can think of this fork as a test repository, where you can apply the changes on your documentation every time you want to update it but without doing it publicly.

## Create a new GitlabPagesSite instance

Go to [webeos.cern.ch](https://webeos.cern.ch/){target=_blank} and follow the same procedure as if you [were to create a new site](create_site.md){target=_blank}. Remember to use a different host for the new site (e.g. `my-docs-review.docs.cern.ch`).

## Merge the changes upstream

Once you are happy with the result, you can [merge your changes upstream](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream){target=_blank}, which will update your main repository and automatically redeploy your documentation site with the latest changes.
