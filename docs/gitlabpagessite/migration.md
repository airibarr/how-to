# Migration from the old infrastructure to the new GitLab Pages based static sites

This section will help you migrate your MkDocs site to the new infrastructure.

## Build artifacts

In order to have the changes automatically deployed we need to build the MkDocs documentation artifacts and push them to S3. If you want an easy and automatic way to do it we recommend you to [include a special template](https://gitlab.cern.ch/authoring/documentation/mkdocs-ci/-/blob/master/mkdocs-gitlab-pages.gitlab-ci.yml){target=_blank} on top of your `.gitlab-ci.yml` as you can see in the [example](https://gitlab.cern.ch/authoring/documentation/mkdocs-container-example/-/blob/master/.gitlab-ci.yml){target=_blank}. This will build and push (to a special S3 bucket) static assets every time a new commit is pushed to the `master` branch (or a branch is merged to `master` branch). You can modify it to your liking.

If everything goes as expected you will see a new job in the CI/CD -> Pipelines page:

![pipelines](/images/gitlab-pipelines-migration.png)

!!! note
    With this new setup all your future changes will be automatically deployed once the Gitlab CI job rebuilds your static site with your latest changes.

!!! warning
    `pages` job is supposed to be run only in the event of changes added to `master`. If it runs for a different branch, the documentation hosted on one of your custom domains will be updated as well and that might lead to the changes being visible to everyone accessing your site.

## Create a new project on [webeos.cern.ch](https://webeos.cern.ch/){target=_blank}

You have to create a webEOS project and to do this, go to [webEOS](https://webeos.cern.ch/){target=_blank} and create a new project, checking on the left side menu that the **Administrator** option is selected (and **not** Developer).

This will show a form with the following fields:

* **Name:** This is the name of your project. This field is mandatory. It is good practice to use the same name as your GitLab's repository name, and ending with `-docs` (i.e. my-service-name-docs).
* **Display Name**: Enter the display name of the project.
* **Description**: Enter a description of your site. This field is mandatory.


## Create a site

Once your project is created, on the left bar do one of the following:

* go to the **Operators** -> **Installed Operators** if you are in the `Administrator` view
* or navigate to **+Add** -> **Operator backed** if you are in the `Developer` view

This will show the list of all available operators. Make sure that you are in the correct `Project`.

![installed_operators](/images/openshift-project-installed-operators.png)

Search for `GitLab Pages Site Operator` and click on it. This will open the operator description, similar to the following image:

![operator](/images/openshift-project-create-site-operator.png)

To create your new site click on `Create instance` on the bottom of the box or go to the **Publish a static site from a Gitlan repository (gitlab.cern.ch)** tab in the top bar. This will show the list of your sites created in this project and a `Create GitlabPagesSites` button in the top-right part. Click on this button and it will open a form with the following fields:

* **Name:** This is the name of your site. Since this name has to be unique we recommend you to use the same name as you have used as your GitLab's repository name.
* **Labels (optional):** Optional labels to give more information about your site. You can leave it empty.
* **Host:** This is the custom domain that you will create in the [next step](migration.md#how-to-add-a-custom-domain) in the repository's GitLab Pages section and hence, the URL of your site, for example `migration-example.docs.cern.ch`. **Notice** that you do not have to put the `http/s` protocol at the beginning.
* **Anonymous:** It is a true/false toggle that will make your site accessible without authentication. If false it will redirect users to the CERN Single Sign-On in order to authenticate them before accessing your site.


!!! warning
    Remember that the `Host` has to be the same as one of the defined GitLab Pages custom domains, hence the same restrictions apply in order to create your hostname's site:

    * Domain (`migration-example` in the previous image) only allows the letters, numbers and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


Once you are done, click on **Create** and your new site will be shown in the list of your project's sites.

## How to enable Pages in a repository

The current repository that contains your documentation files will have a new `Settings` option called `Pages`

![settings](/images/gitlab-settings-pages.png)

Notice that to be able to see `Settings` you have to have, at least, a Maintainer role.

## How to add a custom domain

First of all, make sure that `Force HTTPS (requires valid certificates)` is ticked off and then click on the `Save changes` blue button. Your site will be accessible via HTTPS in any case thanks to the new infrastructure.

![certificates](/images/gitlab-https-pages.png)

In order to create your custom site's URL you have to click on the upper right button **New Domain**. Now, to create your domain please, use the current URL that is used by the old site.

![domain](/images/gitlab-domain-pages.png)

!!! warning
    There are some restrictions in order to create your domain:

    * Domain (`migration-example` in the previous image) only allows the letters, numbers and hyphen symbol ( - ).
    * No hostname can live under a domain that is not supported (`docs.cern.ch` and `web.cern.ch` are allowed; we strongly suggest to use `docs.cern.ch` for new sites and `web.cern.ch` for sites that used the domain in the old infra)
    * No hostname can have its first segment being shorter than 4 characters


You can leave the **Certificate (PEM)** and **Key (PEM)** fields empty if you have previously disabled the button `Force HTTPS (requires valid certificates)`.

Click on **Create New Domain** button and then on the button **Save Changes**. If everything goes well you will see the new domain in the `Domains` list. If everything goes well your new site should be available after a few minutes, so you can open your browser and type the URL you put as `Host` in [this section](create_site.md#create-a-site) to see your new site.

## Keep it clean

Some things are not necessary anymore therefore you should remove them (e.g. deploy token, webhook definition).

### Remove webhook

Go to your git repository on [GitLab](https://gitlab.cern.ch/){target=_blank} and hover over **Settings** on the left panel. Click on **Webhooks** option and remove the webhook that have been used to trigger Openshift builds.

![Screenshot](/images/gitlab-settings_integrations-do-1.png)

### Remove deploy token if it had been created before

Go to your git repository on [GitLab](https://gitlab.cern.ch/){target=_blank} and hover over **Settings** on the left panel. Click on **Repository** option.

![Screenshot](/images/gitlab-settings_repository-do.png)

Now, expand the **Deploy Tokens** section and remove the token that have been used for the Openshift builds.

![Screenshot](/images/gitlab-settings_repository-deploy_tokens-do.png)


### Remove your old OpenShift 3 project

Go to the [Web Services portal](https://webservices.web.cern.ch) and click on the **My websites** tab. Here you will see all your websites, just click on the website that you want to remove and click on `Remove name-of-your-website` in the `Toolbox for current site`.
