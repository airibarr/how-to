# Temporarily review your documentation on OpenShift before deploying it

After updating your documentation in your git repository it is automatically redeployed on OpenShift ([provided that you have followed all the steps of this how-to guide](../../new/redeploy.md)).

It is possible that you want to temporarily review what your updates look like before automatically deploying them. This advanced how-to guide presents a workflow in which:

* You work on your documentation updates in a local development branch in your local git repository;
* When ready, you push your local development branch to your remote git repository;
* A temporary review deployment is created on OpenShift through your git repository CI pipeline;
* When ready, you stop the temporary review deployment through your git repository CI pipeline;
* You merge your documentation updates to the `master` branch in your git repository and they are automatically deployed on OpenShift.

!!! warning
    Note that temporary review deployments are by default publicly accessible in the CERN intranet. At the same time they are not publicly indexed anywhere and one needs to know what exact URL to access them at.

If this workflow sounds interesting to you:

1. [First, you need to set it up](setup.md).
2. [Then, learn how to use it](use.md).
3. [Finally, extend it according to your needs](extend.md).
